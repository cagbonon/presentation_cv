
import 'package:flutter/material.dart';
import 'package:presentation_cv/widget.dart';


void main() {
  
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Presatation des CV',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Presatation des CV'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    //double hdt = MediaQuery.of(context).size.height;
    double wdt = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(wdt / 16, 30, wdt / 16, 0),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Row(
              children: [
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
              ],
            ),
            SizedBox(
              height: wdt / 16,
            ),
            Row(
              children: [
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
              ],
            ),
            SizedBox(
              height: wdt / 16,
            ),
            Row(
              children: [
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
              ],
            ),
            SizedBox(
              height: wdt / 16,
            ),
            Row(
              children: [
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
                SizedBox(
                  width: wdt / 16,
                ),
                cardredoc(context, "Descriptions"),
              ],
            ),
          ],
        )),
      ),
    );
  }
}
