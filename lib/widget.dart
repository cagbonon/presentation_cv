import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:presentation_cv/show.dart';
import 'package:universal_html/html.dart' as html;
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

Widget cardredoc(BuildContext context, String description) {
  double hdt = MediaQuery.of(context).size.height;
  double wdt = MediaQuery.of(context).size.width;
  final pdf = pw.Document();
  pdf.addPage(pw.Page(
      pageFormat: PdfPageFormat.a4,
      build: (pw.Context context) {
        return pw.Center(
          child: pw.Text("Hello World"),
        );
      }));
  final bytes = pdf.save();
  final blob = html.Blob([bytes], 'application/pdf');

  return Container(
    decoration: BoxDecoration(
      //color: Colors.blue,
      border: Border.all(
        color: Colors.grey,
        width: 1,
      ),

      borderRadius: BorderRadius.circular(10),
    ),
    height: hdt / 5,
    width: wdt / 4,
    child: Container(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: (hdt / 45) / 2,
            ),
            Container(
              height: (hdt / 45) * 4,
              width: (wdt / 4) - 16,
              decoration: BoxDecoration(
                //color: Colors.blue,
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            SizedBox(
              height: (hdt / 90) / 2,
            ),
            Center(child: Text(description)),
            SizedBox(
              height: (hdt / 90) / 2,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                    onPressed: () {
                      // MaterialPageRoute(
                      //   builder: (context) => const showpdf(),
                      // );
                      final url = html.Url.createObjectUrlFromBlob(blob);
                      html.window.open(url, "_blank");
                      html.Url.revokeObjectUrl(url);
                    },
                    child: const Text("Voir")),
                IconButton(
                    onPressed: () {
                      final url = html.Url.createObjectUrlFromBlob(blob);
                      final anchor =
                          html.document.createElement('a') as html.AnchorElement
                            ..href = url
                            ..style.display = 'none'
                            ..download = 'cv.pdf';
                      html.document.body!.children.add(anchor);
                      anchor.click();
                      html.document.body!.children.remove(anchor);
                      html.Url.revokeObjectUrl(url);
                    },
                    icon: const Icon(
                      Icons.download,
                      color: Colors.blue,
                    ))
              ],
            ),
            SfPdfViewer.asset('assets/doc/doc.pdf'),
          ],
        ),
      ),
    ),
  );
}


