import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';

class showpdf extends StatefulWidget {
  const showpdf({super.key});

  @override
  State<showpdf> createState() => _showpdfState();
}

class _showpdfState extends State<showpdf> {
  static const int _initialPage = 1;
  bool _isSampleDoc = true;
  late PdfControllerPinch _pdfControllerPinch;
  @override
  void initState() {
    _pdfControllerPinch = PdfControllerPinch(
      document: PdfDocument.openAsset('assets/doc/doc.pdf'),
      initialPage: _initialPage,
    );
    super.initState();
  }

  @override
  void dispose() {
    _pdfControllerPinch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("cv")),
      body: PdfViewPinch( 
        controller: _pdfControllerPinch,
      ),
    );
  }
}
